import React from 'react';
import { TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import { Card as RNCard } from 'react-native-elements';


export default Rewards = (props) => {
    return (
        props.list.map((item,key) => (
            <TouchableOpacity onPress={item.onPress} style={styles.cardWrapper}>
            <RNCard imageStyle={styles.cardImage} containerStyle={styles.card} image={{ uri: item.imageUrl }}>
                <Text style={styles.cardHeader}>{item.header}</Text>
                <Text style={styles.cardBody}>{item.body}</Text>
                <Text style={styles.cardFooter}>{item.footer}</Text>
            </RNCard>
        </TouchableOpacity>
        ))
    );
}

const styles = StyleSheet.create({
    cardWrapper: {
        flexDirection: 'row',
    },
    card: {
        flex: 0.85,
        justifyContent: 'center'
    },
    cardImage: {
        maxWidth:'100%',
        height:100
    },
    cardHeader: {
        color: '#353535',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: '2%'
    },
    cardBody: {
        color: '#6C6C6C',
        textAlign: 'center',
        marginBottom: '2%'
    },
    cardFooter: {
        color: 'green',
        textAlign: 'center',
        marginBottom: '2%',
        fontWeight: '700'
    }
});