import React from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Card as RNCard } from 'react-native-elements';

export default Card = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} style={styles.cardWrapper}>
            <RNCard imageStyle={styles.cardImage} containerStyle={styles.card} image={{ uri: props.imageUrl }}>
                <Text style={styles.cardHeader}>{props.header}</Text>
                <Text style={styles.cardBody}>{props.body}</Text>
                <Text style={styles.cardFooter}>{props.footer}</Text>
            </RNCard>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    cardWrapper: {
        flexDirection: 'row',
    },
    card: {
        flex: 0.85,
        justifyContent: 'center'
    },
    cardImage: {
        width: '40%',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: '5%'
    },
    cardHeader: {
        color: '#353535',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: '2%'
    },
    cardBody: {
        color: '#6C6C6C',
        textAlign: 'center',
        marginBottom: '2%'
    },
    cardFooter: {
        color: 'green',
        textAlign: 'center',
        marginBottom: '2%',
        fontWeight: '700'
    }
});