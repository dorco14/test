import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Card from './Components/Card.js'
import Rewards from './Components/Rewards.js'

const onPress = (text) => alert(text)


export default App = () => {
    return (
            <ScrollView contentContainerStyle={styles.container} >
                <Rewards list={list} />
            </ScrollView>
    );
}
const list = [
    {
        imageUrl: "https://www.drushim.co.il/Logos/1725982/Header/636957773377429999.png",
        header: "Cash out with paypal!",
        body: 'only with 15 coins',
        footer: "15$",
        onPress: () => onPress("cash out with 15$")
    },
    {
        imageUrl: "https://www.drushim.co.il/Logos/1725982/Header/636957773377429999.png",
        header: "Cash out with paypal!",
        body: 'only with 20 coins',
        footer: "20$",
        onPress: () => onPress("cash out with 20$")
    },
    {
        imageUrl: "https://www.drushim.co.il/Logos/1725982/Header/636957773377429999.png",
        header: "Cash out with paypal!",
        body: 'only with 25 coins',
        footer: "25$",
        onPress: () => onPress("cash out with 25$")
    },
]
const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
